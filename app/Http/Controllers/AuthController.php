<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata(){
        return view('halaman.biodata');
    }

    public function kirim(Request $request){
        $fname = $request['fname'];
        $lname = $request['lname'];

        return view('halaman.welcome', compact('fname','lname'));
    }
}
