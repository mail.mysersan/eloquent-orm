<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{
   public function create(){
       return view('cast.create');
   }

   public function store(Request $request){
       $request->validate([
           'nama' => 'required',
           'umur' => 'required',
           'bio'  => 'required',
       ]);
       
       /* Query Builder
       DB::table('cast')->insert(
           [
               'nama' => $request['nama'],
               'umur' => $request['umur'],
               'bio' => $request['bio']
               
           ]
           );
        */

        $cast = new Cast;
        $cast->nama = $request["nama"];
        $cast->umur = $request["umur"];
        $cast->bio = $request["bio"];
        $cast->save();//ORM INSERT DATA

           return redirect ('/cast');
   }

   public function index(){
       //$cast = $users = DB::table('cast')->get();
       $cast = cast::all();//ORM SELECT/TAMPIL ALL
       return view('cast.index', compact('cast'));
    }

    public function show($id){
        //$cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id); //ORM DETAIL/PER_ID
        return view('cast.show', compact('cast'));
    }

    public function edit($id){
        //$cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id); //ORM DETAIL/PER_ID
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate(
        [
           'nama' => 'required',
           'umur' => 'required',
           'bio'  => 'required',
        ]);

        /* 
        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
               'nama' => $request['nama'],
               'umur' => $request['umur'],
               'bio' => $request['bio']
            ]);
         */
        
        $update = Cast::where('id', $id)->update([
               "nama" => $request["nama" ],
               "umur" => $request["umur" ],
               "bio" => $request["bio"]
        ]);//ORM EDIT

        return redirect('/cast');
    }

    public function destroy($id)
    {
        //$query = DB::table('cast')->where('id', $id)->delete();
        Cast::destroy($id);
        return redirect('/cast');
    }
}
