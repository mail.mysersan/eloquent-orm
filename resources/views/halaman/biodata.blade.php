@extends('layouts.master')

@section('title')
    Sign Up Form
@endsection

@section('content')

<form action="/kirim" method="POST">
    @csrf
    <label>Firt Name:</label><br><br>
        <input type="text" name="fname"><br><br>
    <label>Last Name:</label><br><br>
        <input type="text" name="lname"><br><br>
    <label>Gender:</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>
    <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="singapura">Singapure</option>
        </select><br><br>
    <label>Language Spoken:</label><br><br>
        <input type="checkbox"> Bahasa Indonesia<br>
        <input type="checkbox"> English<br>
        <input type="checkbox"> Other<br><br>
    <label>Bio:</label><br><br>
        <textarea name="alamat" cols="30" rows="10"></textarea><br><br>
    <input type="submit" value="kirim">
    </form> 
@endsection
