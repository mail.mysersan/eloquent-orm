<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@home');
Route::get('/form', 'AuthController@biodata');
Route::post('/kirim', 'AuthController@kirim');

Route::get('/data-tables', function(){
    return view('tables.data-tables');
});

Route::get('/tables', function(){
    return view('tables.tables');
});

//CRUD
//direct to form cast create
Route::get('/cast/create', 'CastController@create');
//insert data cast to db
Route::post('/cast', 'CastController@store');
//select data cast
Route::get('/cast', 'CastController@index');
//show detail cast
Route::get('/cast/{cast_id}', 'CastController@show');
//goto form cast edit
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//edit cast by id
Route::put('/cast/{cast_id}', 'CastController@update');
//del cast by id
Route::delete('/cast/{cast_id}', 'CastController@destroy');